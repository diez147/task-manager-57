package ru.tsc.babeshko.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.Project;

public interface IProjectService extends IUserOwnedService<Project> {

    void removeAllByUserId(@Nullable String userId);

}
